#!/usr/bin/perl

#
# GBeXtrack.pl
#
# Task:
# extract BED coverage from BAM files and
# prepare a Genome Browser Track file
#
# Run:
# perl GBeXtrack.pl --help
#
# Author: Georgi Tushev
# Scientific Computing Facility
# Max-Planck Institute for Brain Research
# Frankfurt am Main, Germany
# Bug reports: sciclist@brain.mpg.de
#

use warnings;
use strict;
use Getopt::Long();

# declare subroutines
sub usage($);
sub parseBedFile($$);
sub exonsMerge($);
sub extractTrackDepth($$);
sub linearTrackDepth($$$$$);
sub normalizeTrackDepth($$);
sub printLinearTrack($$$$$$);

# main
main :{
    my $query_name;
    my $file_bed;
    my @files_bam;
    my @factors_norm;
    my $version = "version 0.0.1";
    my $help;

    Getopt::Long::GetOptions(
     "q|query=s" => \$query_name,
     "a|bed=s" => \$file_bed,
     "b|bams=s{1,}" => \@files_bam,
     "s|sizes=s{1,}" => \@factors_norm,
     "h|help" => \$help,
    ) or usage("GBeXtrack: error, invalid command line option\n");

    usage("GBeXtrack $version\n") if(defined($help));
    usage("GBeXtrack: error, query gene symbol is required\n") unless(defined($query_name));
    usage("GBeXtrack: error, annotation file is required\n") unless(defined($file_bed));
    usage("GBeXtrack: error, alignment file is required\n") unless(@files_bam);
    @factors_norm = (1000000) x scalar(@files_bam) if (!defined($factors_norm[0]));
    usage("GBeXtrack: error, number of normalization factors should match number of BAM files") unless (scalar(@files_bam) == scalar(@factors_norm));

    # define track data
    my ($chrom, $strand, $span, $exons, $orfs) = parseBedFile($query_name, $file_bed);
    
    # extract coverage
    my $query_region = $chrom . ":" . $exons->[0][0] . "-" . $exons->[-1][1];
    my $depth_raw = extractTrackDepth($query_region, \@files_bam);
    my $depth_linear = linearTrackDepth($strand, $span, $exons, $depth_raw, scalar(@files_bam));
    my ($depth_rpm, $depth_max) = normalizeTrackDepth($depth_linear, \@factors_norm);
    
    # print main track
    my $trackLimit = $span + 100;
    print "browser hide all\n";
    print "browser position chrX:1-$trackLimit\n";
    print "track type=bed name=$query_name desription=\"transcript linear form\" color=0,0,0 visibility=full\n";
    foreach my $track (@{$orfs}) {
        print "chrX","\t",0,"\t",$span,"\t",$track->[0],"\t",0,"\t","+","\t",$track->[1],"\t",$track->[2],"\n";
    }

    # print tracks
    printLinearTrack("full", $depth_rpm, 0, $span, $depth_max, "220,220,220");
    foreach my $orf (@{$orfs}) {
        next if($orf->[1] == $orf->[2]);
        my $rgbColor = int(rand(256)) . "," . int(rand(256)) . "," . int(rand(256));
        printLinearTrack($orf->[0], $depth_rpm, $orf->[1], $orf->[2], $depth_max, $rgbColor);
    }

    
}


# subroutines

# USAGE
sub usage($) {
    my $message = $_[0];
    if (defined $message && length $message)
    {
        $message .= "\n" unless($message =~ /\n$/);
    }
    
    my $command = $0;
    $command =~ s#^.*/##;
    
    print STDERR (
        $message,
        "usage: $command --bed annotation.bed -bams runA.bam runB.bam --query Gene\n" .
        "description: GBeXtrack creates a BedGraph track file with linear gene representation (no introns).\n" .
        "parameters:\n" .
        "-q|--query\n" .
        "\tquery gene name\n" .
        "-b|--bed\n" .
        "\tvalid annotation file in BED12 format\n" .
        "-b|--bams\n" .
        "\tlist of valid alignments in BAM format\n" .
        "-s|--sizes\n" .
        "\tlist of library size, [default = no normalization]\n" .
        "-help\n" .
        "\tdefine usage\n"
    );
    
    die("\n");
}


# PARSEBEDFILE
sub parseBedFile($$) {
    my $query_name = $_[0];
    my $file_bed = $_[1];
    
    my $chrom;
    my $strand;
    my @exons = ();
    my @orfs = ();
    
    open (my $fh, "grep \"$query_name\" $file_bed|") or die $!;
    while (<$fh>) {
        chomp($_);
        my @bed = split("\t", $_, 12);
        $chrom = $bed[0];
        my $chromStart = $bed[1];
        my $name = $bed[3];
        $strand = $bed[5];
        my $thickStart = $bed[6];
        my $thickEnd = $bed[7];
        my $blocks = $bed[9];
        my @blockSizes = split(",", $bed[10]);
        my @blockStarts = split(",", $bed[11]);
        for (my $e = 0; $e < $blocks; $e++) {
            
            # add exons
            my $exonStart = $chromStart + $blockStarts[$e];
            my $exonEnd = $exonStart + $blockSizes[$e];
            push(@exons, [$exonStart, $exonEnd]);
        }
        # add orfs
        push(@orfs, [$name, $thickStart, $thickEnd]);
    }
    close($fh);
    
    
    # check if search was empty
    if (!defined($chrom)) {
        print STDERR "GBeXtrack: error, failed to find gene annotation in BED file for ", $query_name,"\n";
        exit(1);
    }
    
    # merge exons
    my ($exons_merged, $span) = exonsMerge(\@exons);
    
    # identify linear orf positions
    my $blocks = scalar(@{$exons_merged});
    my @orfs_linear = ();
    foreach my $orf (@orfs) {
        my $name = $orf->[0];
        my $thickStart = $orf->[1];
        my $thickEnd = $orf->[2];
        next if ($thickStart == $thickEnd);
        my $blockSpan = 0;
        my $orfStart = -1;
        my $orfEnd = -1;
        for (my $e = 0; $e < $blocks; $e++) {
            my $exonStart = $exons_merged->[$e][0];
            my $exonEnd = $exons_merged->[$e][1];
            if (($exonStart <= $thickStart) && ($thickStart <= $exonEnd)) {
                $orfStart = $thickStart - $exonStart + $blockSpan;
            }
            
            if (($exonStart <= $thickEnd) && ($thickEnd <= $exonEnd)) {
                $orfEnd = $thickEnd - $exonStart + $blockSpan;
            }
            
            $blockSpan += ($exonEnd - $exonStart);
        }
        
        # flip strand
        if ($strand eq "-") {
            $orfStart = $blockSpan - $orfStart;
            $orfEnd = $blockSpan - $orfEnd;
            ($orfStart, $orfEnd) = ($orfEnd, $orfStart);
        }

        # add orfs
        push(@orfs_linear, [$name, $orfStart, $orfEnd]);
    }
    
    
    
    return ($chrom, $strand, $span, $exons_merged, \@orfs_linear);
}


# EXONSMERGE
sub exonsMerge($)
{
    my $exons = $_[0];
    my $blocks = scalar(@{$exons});
    
    # sort array
    @{$exons} = sort {$a->[1] <=> $b->[1]} @{$exons};
    my $prevStart = $exons->[0][0];
    my $prevEnd = $exons->[0][1];
    my @exonsMerged;
    my $span = 0;
    for(my $e = 1; $e < $blocks; $e++) {
        
        my $exonStart = $exons->[$e][0];
        my $exonEnd = $exons->[$e][1];
        
        if (($prevStart < $exonEnd) & ($exonStart < $prevEnd)) {
            $prevStart = ($prevStart <= $exonStart) ? $prevStart : $exonStart;
            $prevEnd = ($prevEnd <= $exonEnd) ? $exonEnd : $prevEnd;
            
        }
        else {
            push(@exonsMerged, [$prevStart, $prevEnd]);
            $span += ($prevEnd - $prevStart);
            $prevStart = $exonStart;
            $prevEnd = $exonEnd;
        }
    }
    push(@exonsMerged, [$prevStart, $prevEnd]);
    $span += ($prevEnd - $prevStart);
    return (\@exonsMerged, $span);
}


# EXTRACTRACKDETPH
sub extractTrackDepth($$) {
    my $region = $_[0];
    my $files_bam = join(" ", @{$_[1]});
    my %depth = ();
    open (my $fh, "samtools depth -Q 255 -d 0 -r $region $files_bam|") or die $!;
    while (<$fh>) {
        chomp($_);
        my ($chrom, $position, @cov) = split("\t", $_);
        my $cov_sum = 0;
        foreach my $value (@cov) {
            $cov_sum += $value;
        }
        next if ($cov_sum == 0);
        $depth{$position} = \@cov;
    }
    close($fh);
    return \%depth;
}


# LINEARTRACKDEPTH
sub linearTrackDepth($$$$$) {
    my $strand = $_[0];
    my $span = $_[1];
    my $exons = $_[2];
    my $depth_raw = $_[3];
    my $reps = $_[4];
    
    # allocate linear array
    my @depth_linear = ();
    for (my $i = 0; $i < $span; $i++) {
        my @defaultValue = (0) x $reps;
        $depth_linear[$i] = \@defaultValue;
    }
    
    # fill depth
    my $offset = 0;
    my $blocks = scalar(@{$exons});
    for (my $e = 0; $e < $blocks; $e++) {
        my $exonStart = $exons->[$e][0];
        my $exonEnd = $exons->[$e][1];
        for (my $base = $exonStart + 1; $base <= $exonEnd; $base++) {
            my $index = ($base - $exonStart) + $offset;
            next if (($index < 0) || ($span <= $index));
            $depth_linear[$index] = $depth_raw->{$base} if (exists($depth_raw->{$base}));
        }
        $offset += ($exonEnd - $exonStart);
    }
    
    @depth_linear = reverse(@depth_linear) if ($strand eq "-");
    
    return (\@depth_linear);
}


# NORMALIZETRACKDEPTH
sub normalizeTrackDepth($$) {
    my $depth_linear = $_[0];
    my $factors = $_[1];
    
    my $reps = scalar(@{$factors});
    my $span = scalar(@{$depth_linear});
    my @depth_normed = (0) x $span;
    my $depth_max = 0;
    for (my $k = 0; $k < $span; $k++) {
        my $depth_now = 0;
        for (my $i = 0; $i < $reps; $i++) {
            my $fvalue = 1000000 / $factors->[$i]; # reads per million
            $depth_now += $depth_linear->[$k][$i] * $fvalue * (1/$reps);
        }
        $depth_normed[$k] = $depth_now;
        $depth_max = $depth_normed[$k] if ($depth_max < $depth_normed[$k]);
    }
    
    $depth_max = int($depth_max + 0.5);
    return (\@depth_normed, $depth_max);
}


# PRINTLINEARTRACK
sub printLinearTrack($$$$$$)
{
    my $name = $_[0];
    my $depth = $_[1];
    my $depthStart = $_[2];
    my $depthEnd = $_[3];
    my $depthMax = $_[4];
    my $rgbColor = $_[5];
    
    # bedGraph format
    my $track;
    my $idxStart = $depthStart;
    my $value = $depth->[$idxStart];
    my $span = scalar(@{$depth});
    $span = $depthEnd if ($depthEnd < $span);
    for (my $i = $idxStart + 1; $i < $span; $i++) {
        if ($value != $depth->[$i]) {
            $track .= "chrX\t" . $idxStart . "\t" . $i . "\t" . $value . "\n" if ($value > 0);
            $idxStart = $i;
            $value = $depth->[$i];
        }
    }
    $track .= "chrX\t" . $idxStart . "\t" . $span . "\t" . $value . "\n" if ($value > 0);
    
    # print track
    if (defined($track)) {
        print "track type=bedGraph name=$name visibility=full color=$rgbColor autoScale=off viewLimits=0:$depthMax windowingFunction=mean smoothingWindow=2\n";
        print $track;
    }
    
    
}
