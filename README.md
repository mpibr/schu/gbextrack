# GBeXtrack

extract BED coverage from BAM files and prepare a Genome Browser Track file for visualisation at [UCSC Genome Browser](https://genome.ucsc.edu/cgi-bin/hgCustom)

## prerequisite
* Unix / Linux system
* [grep](https://en.wikipedia.org/wiki/Grep)
* [samtools depth](http://www.htslib.org/)

## execute

* create a track file
```
perl GBeXtrack.pl --query GeneX --bed annotation.bed --bams $(ls ./bams/*.bam) --sizes 1000000 2000000 > gbTrack_GeneX.txt
```

* upload the file as a [custom track](https://genome.ucsc.edu/cgi-bin/hgCustom)

## help

```
GBeXtrack version 0.0.1
usage: GBeXtrack.pl --bed annotation.bed --bams runA.bam runB.bam --query Gene
description: GBeXtrack creates a BedGraph track file with linear gene representation (no introns).
parameters:
-q|--query
	query gene name
-b|--bed
	valid annotation file in BED12 format
-b|--bams
	list of valid alignments in BAM format
-s|--sizes
	list of library size, [default = no normalization]
-help
	define usage
```